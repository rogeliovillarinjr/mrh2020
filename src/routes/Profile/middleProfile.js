import React from "react";
// import RadialGraph from "./radialGraph";
// import RadialGraphVisayas from "./radialGraphVisayas";
// import RadialGraphMindanao from "./radialGraphMindanao";
import BarGraph from "./barGraph";
import StackGraph from "./stackChart";

const middleProfile = () => {
    return (
        <div>
            <div className="middleprofilePage">
                <p className="topTitle">Dashboard</p>
                <div className="row section01">
                    <div className="col-md-2 list">
                        <p className="total">150</p>
                        <p className="prgress">
                            <progress max="100" value="50" ></progress>
                        </p>
                        <p className='title'>
                            Total Properties
                    </p>
                    </div>
                    <div className="col-md-2 list">
                        <p className="total">350</p>
                        <p className="prgress">
                            <progress max="100" value="50" ></progress>
                        </p>
                        <p className="title">
                            New Properties
                    </p>
                    </div>
                    <div className="col-md-2 list">
                        <p className="total">550</p>
                        <p className="prgress">
                            <progress max="100" value="50" ></progress>
                        </p>
                        <p className="title">
                            Project Listed
                    </p>
                    </div>
                    <div className="col-md-2 list">
                        <p className="total">250</p>
                        <p className="prgress">
                            <progress max="100" value="50" ></progress>
                        </p>
                        <p className="title">
                            Property View
                    </p>
                    </div>
                    <div className="col-md-2 list">
                        <p className="total">750</p>
                        <p className="prgress">
                            <progress max="100" value="50" ></progress>
                        </p>
                        <p className="title">
                            Property Sold
                    </p>
                    </div>
                </div>
                <div className="row monthlyRepoort">
                    <p className="mTitle">Monthly Report</p>
                    <div className="row mList">
                        <div className="col-md-3"><p>Php 150<br /><span>Todays</span></p></div>
                        <div className="col-md-3"><p>Php 150<br /><span>This Week's</span></p></div>
                        <div className="col-md-3"><p>Php 150<br /><span>This Month's</span></p></div>
                        <div className="col-md-3"><p>Php 150<br /><span>This Year's</span></p></div>
                    </div>
                    <StackGraph />
                </div>
                <div className="row propertyView">
                    <p className="proView">Property View</p>
                    <div className="row pList">
                        <div className="col-md-4"><p>Overall<br /><span>70-50%</span></p></div>
                        <div className="col-md-4"><p>Monthly<br /><span>70-50%</span></p></div>
                        <div className="col-md-4"><p>Day<br /><span>70-50%</span></p></div>
                    </div>
                    <BarGraph />
                </div>
                <div className="row propertyView">
                    <p className="proView">Statistics by Region</p>
                    <div className="row pProper">
                        <div className="col-md-4">
                            <div className="set-size charts-container">
                                <div className="pie-wrapper progress-45 style-2">
                                    <span className="label">45<span className="smaller"></span></span>
                                    <div className="pie">
                                        <div className="left-side half-circle"></div>
                                        <div className="right-side half-circle"></div>
                                    </div>
                                    <div className="shadow"></div>
                                </div>
                            </div>
                            <p>Luzon</p>
                        </div>
                        <div className="col-md-4">
                            <div className="set-size charts-container">
                                <div className="pie-wrapper progress-75 style-2">
                                    <span className="label">75<span className="smaller"></span></span>
                                    <div className="pie">
                                        <div className="left-side half-circle"></div>
                                        <div className="right-side half-circle"></div>
                                    </div>
                                    <div className="shadow"></div>
                                </div>
                            </div>
                            <p>Visayas</p>
                        </div>
                        <div className="col-md-4">
                            <div className="set-size charts-container">
                                <div className="pie-wrapper progress-95 style-2">
                                    <span className="label">95<span className="smaller"></span></span>
                                    <div className="pie">
                                        <div className="left-side half-circle"></div>
                                        <div className="right-side half-circle"></div>
                                    </div>
                                    <div className="shadow"></div>
                                </div>
                            </div>
                            <p>Mindanao</p>
                        </div>
                    </div>

                </div>
            </div>
            &#169; Copyright 2020. My RealHub | All Rights Reserved.
        </div>
    );
};

export default middleProfile;
