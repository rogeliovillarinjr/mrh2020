import React from "react";

const RightProfile = () => {
    return (
        <div>
            <div className="profilePageRight">
                <div className="row">
                    <div className="header">
                        LIVE UPDATES
                    </div>
                </div>
                <div className="row">
                    <div className="newproperties">
                        <div className="title">
                            <div>NEW PROPERTIES</div>
                        </div>
                        <div className="row properList">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-4">
                                        <div><img src={require('../../images/profile/ed-1.png')} alt="" /></div>
                                    </div>
                                    <div className="col-md-8 info">
                                        <div className="row name">
                                            <div>John Doe</div>
                                        </div>
                                        <div className="row desig">
                                            <div>Broker</div>
                                        </div>
                                        <div className="row date">
                                            <div>5/20/20</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 right">
                                <div className="row propertype">
                                    <div>Commercial Type</div>
                                </div>
                                <div className="row forsale">
                                    <div>For Sale</div>
                                </div>
                                <div className="row price">
                                    <div>Php 2.15m</div>
                                </div>
                            </div>
                        </div>
                        <div className="row properList">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-4">
                                        <div><img src={require('../../images/profile/ed-1.png')} alt="" /></div>
                                    </div>
                                    <div className="col-md-8 info">
                                        <div className="row name">
                                            <div>John Doe</div>
                                        </div>
                                        <div className="row desig">
                                            <div>Broker</div>
                                        </div>
                                        <div className="row date">
                                            <div>5/20/20</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 right">
                                <div className="row propertype">
                                    <div>Commercial Type</div>
                                </div>
                                <div className="row forsale">
                                    <div>For Sale</div>
                                </div>
                                <div className="row price">
                                    <div>Php 2.15m</div>
                                </div>
                            </div>
                        </div>
                        <div className="row properList">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-4">
                                        <div><img src={require('../../images/profile/ed-1.png')} alt="" /></div>
                                    </div>
                                    <div className="col-md-8 info">
                                        <div className="row name">
                                            <div>John Doe</div>
                                        </div>
                                        <div className="row desig">
                                            <div>Broker</div>
                                        </div>
                                        <div className="row date">
                                            <div>5/20/20</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 right">
                                <div className="row propertype">
                                    <div>Commercial Type</div>
                                </div>
                                <div className="row forsale">
                                    <div>For Sale</div>
                                </div>
                                <div className="row price">
                                    <div>Php 2.15m</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="regions">
                        <div className="title">
                            <div>REGIONS</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">Luzon</div>
                            <div className="col-md-5 total">1,850</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">Visayas</div>
                            <div className="col-md-5 total">2,850</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">Mindanao</div>
                            <div className="col-md-5 total">3,850</div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="regions">
                        <div className="title">
                            <div>GLOBAL PROPERTIES</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">Total</div>
                            <div className="col-md-5 total">1,230</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">New</div>
                            <div className="col-md-5 total">1,230</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">Listed</div>
                            <div className="col-md-5 total">1,230</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">View</div>
                            <div className="col-md-5 total">1,230</div>
                        </div>
                        <div className="row">
                            <div className="col-md-7 country">Sold</div>
                            <div className="col-md-5 total">1,2301</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default RightProfile;
