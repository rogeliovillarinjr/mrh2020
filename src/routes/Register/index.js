import React from "react";

const Index = () => {
    return (<div>
        <div className="mrh_login">
            <div className="container">
                <div className="row">
                    <div className="login">
                        <img src="https://myrealhub.000webhostapp.com/images/login/logo.png" alt="" />
                    </div>
                    <div className="needhelp">
                        <a href="/#" className="fb"><img src="https://myrealhub.000webhostapp.com/images/login/fb.png" alt="" /></a>
                        <a href="/#" className="tw"><img src="https://myrealhub.000webhostapp.com/images/login/tw.png" alt="" /></a>
                        <a href="/#" className="insta"><img src="https://myrealhub.000webhostapp.com/images/login/insta.png" alt="" /></a>
                        <a href="/#" className="signin">SIGN IN</a>
                    </div>
                </div>
                <div className="signupform">
                    <div className="rowx">
                        <div className="logo">
                            <img src="https://myrealhub.000webhostapp.com/images/login/LOGOfav.png" alt="" />
                        </div>
                        <div className="rowx">
                            <input type="text" className="username" placeholder="Enter Username" />
                        </div>
                        <div className="rowx">
                            <input type="text" className="email" placeholder="Enter Email" />
                        </div>
                        <div className="rowx">
                            <input type="text" className="password" placeholder="Password" />
                        </div>
                        <div className="rowx">
                            <input type="text" className="password" placeholder="Confirm Password" />
                        </div>
                        <div className="checkbox">
                            <input type="checkbox" className="checkbox" /> I read and agree to the <a href="/#">terms of usage</a>
                        </div>
                        <div className="rowx">
                            <input type="submit" placeholder="SUBMIT" />
                        </div>
                        <div className="memb">
                            YOU ALREADY HAVE A MEMBERSHIP?
            </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="row mrh_login_footer">
            <div className="container">
                <div className="fmenu">
                    <a href="/#">
                        CONTACT US
        </a>
                    <a href="/#">
                        ABOUT US
        </a>
                    <a href="/#">
                        FAQ
        </a>
                </div>
                <div className="copyr">
                    &#169; Copyright 2020 | All Rights Reserved | Powered by MyRealHub
    </div>
            </div>
        </div>
    </div>);
};

export default Index;
