import React from "react";
import { Link } from "react-router-dom";
import "../../App.css";

const Navbar = () => {
  return (
    <div>
      <section
        className="navbar custom-navbar"
        role="navigation"
        style={{ background: "#fff", paddingBottom: "20px" }}
      >
        <div className="top_header" style={{ backgroundColor: "#586f5f" }}>
          <div className='container'>
            <div className="col-sm-2 email_wrap">
              <img src="https://myrealhub.000webhostapp.com/images/email.png" alt="" />
              <a href="/#" className="email">
                info@myrealhub.com
              </a>
            </div>
            <div className="col-sm-3">
              <img src="https://myrealhub.000webhostapp.com/images/number.png" alt="" />
              <a href="/#" className="number">
                1-800-1234-567
              </a>
            </div>
            <div className="col-sm-3">
              <img src="https://myrealhub.000webhostapp.com/images/login.png" alt="" />
              <Link to="/login" className="login">
                LOG IN /
              </Link>{" "}
              <Link to="/register" className="">REGISTER</Link>
            </div>
            <div className="col-sm-4 list_proper">
              <Link to="/properties" className="list">
                LIST OF PROPERTIES
              </Link>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="navbar-header">
            <button
              className="navbar-toggle"
              data-toggle="collapse"
              data-target=".navbar-collapse"
            >
              <span className="icon icon-bar"></span>
              <span className="icon icon-bar"></span>
              <span className="icon icon-bar"></span>
            </button>

            <Link to="/" className="smoothScroll">
              <img
                className="logo"
                src="https://myrealhub.000webhostapp.com/images/logo.png"
                width="200"
                alt=""
              />
            </Link>
          </div>

          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav navbar-nav-first">
              <li>
                <Link to="/" className="smoothScroll">
                  HOME
                </Link>
              </li>
              <li>
                <Link to="/about" className="smoothScroll">
                  ABOUT US
                </Link>
              </li>
              <li>
                <Link to="/blog" className="smoothScroll">
                  BLOG
                </Link>
              </li>
              <li>
                <Link to="/newdevelopment" className="smoothScroll">
                  New Development
                </Link>
              </li>
              <li>
                <Link to="/properties" className="smoothScroll">
                  PROPERTIES
                </Link>
              </li>
              <li>
                <Link to="/exploreplaces" className="smoothScroll">
                  EXPLORE PLACES
                </Link>
              </li>
              <li>
                <Link to="/featureddevelopers" className="smoothScroll">
                  FEATURED DEVELOPERS
                </Link>
              </li>
              <li className="dropdown"><a className="dropdown-toggle smoothScroll" data-toggle="dropdown" href="/#">Resources<span className="caret"></span></a>
                <ul className="dropdown-menu">
                  <li><Link to="/resourcesbuyer" className="smoothScroll">BUYER</Link></li>
                  <li><Link to="/resourcesseller" className="smoothScroll">SELLER</Link></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div >
  );
};

export default Navbar;
