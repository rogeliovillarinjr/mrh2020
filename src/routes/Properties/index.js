import React from "react";
import { Link } from "react-router-dom";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const Properties = () => {
    return (
        <div>
            <section id="home" className="banner newdev_banner" data-stellar-background-ratio="1">
                <div className="row">
                    <img src="https://myrealhub.000webhostapp.com/images/propertypage/banner.jpg" alt="" />
                </div>
            </section>


            <div className="home_search_properties wow fadeInUp" data-wow-delay="0.5s">
                <Search />
            </div>
            <div className="property_page01">
                <div className="container">
                    <img src="https://myrealhub.000webhostapp.com/images/propertypage/title01.png" alt="" className="wow fadeInUp" data-wow-delay="0.6s" />
                    <p className="wow fadeInUp" data-wow-delay="0.7s">
                        Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem
     		        </p>
                </div>
            </div>

            <div className="property_page02">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                            <img src="https://myrealhub.000webhostapp.com/images/propertypage/img01.jpg" alt="" />
                            <h1 className="title">BUILDING: 123 ON THE PARk </h1>
                            <div className="price">&#8369; 4,399/<small>month</small></div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon01.jpg' className='thumbnail' alt="" /><label>1200 sq. ft.</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon02.jpg' className='thumbnail' alt="" /><label>4 bedrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon03.jpg' className='thumbnail' alt="" /><label>2 bathrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon04.jpg' className='thumbnail' alt="" /><label>2 garages</label>
                            </div>
                            <div className="property_btn">
                                <Link to="/propertiesdetails" className="smoothScroll">VIEW DETAILS</Link>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                            <img src="https://myrealhub.000webhostapp.com/images/propertypage/img01.jpg" alt="" />
                            <h1 className="title">BUILDING: 123 ON THE PARk </h1>
                            <div className="price">&#8369; 4,399/<small>month</small></div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon01.jpg' className='thumbnail' alt="" /><label>1200 sq. ft.</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon02.jpg' className='thumbnail' alt="" /><label>4 bedrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon03.jpg' className='thumbnail' alt="" /><label>2 bathrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon04.jpg' className='thumbnail' alt="" /><label>2 garages</label>
                            </div>
                            <div className="property_btn">
                                <Link to="/propertiesdetails" className="smoothScroll">VIEW DETAILS</Link>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                            <img src="https://myrealhub.000webhostapp.com/images/propertypage/img01.jpg" alt="" />
                            <h1 className="title">BUILDING: 123 ON THE PARk </h1>
                            <div className="price">&#8369; 4,399/<small>month</small></div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon01.jpg' className='thumbnail' alt="" /><label>1200 sq. ft.</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon02.jpg' className='thumbnail' alt="" /><label>4 bedrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon03.jpg' className='thumbnail' alt="" /><label>2 bathrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon04.jpg' className='thumbnail' alt="" /><label>2 garages</label>
                            </div>
                            <div className="property_btn">
                                <Link to="/propertiesdetails" className="smoothScroll">VIEW DETAILS</Link>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                            <img src="https://myrealhub.000webhostapp.com/images/propertypage/img01.jpg" alt="" />
                            <h1 className="title">BUILDING: 123 ON THE PARk </h1>
                            <div className="price">&#8369; 4,399/<small>month</small></div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon01.jpg' className='thumbnail' alt="" /><label>1200 sq. ft.</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon02.jpg' className='thumbnail' alt="" /><label>4 bedrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon03.jpg' className='thumbnail' alt="" /><label>2 bathrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon04.jpg' className='thumbnail' alt="" /><label>2 garages</label>
                            </div>
                            <div className="property_btn">
                                <Link to="/propertiesdetails" className="smoothScroll">VIEW DETAILS</Link>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                            <img src="https://myrealhub.000webhostapp.com/images/propertypage/img01.jpg" alt="" />
                            <h1 className="title">BUILDING: 123 ON THE PARk </h1>
                            <div className="price">&#8369; 4,399/<small>month</small></div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon01.jpg' className='thumbnail' alt="" /><label>1200 sq. ft.</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon02.jpg' className='thumbnail' alt="" /><label>4 bedrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon03.jpg' className='thumbnail' alt="" /><label>2 bathrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon04.jpg' className='thumbnail' alt="" /><label>2 garages</label>
                            </div>
                            <div className="property_btn">
                                <Link to="/propertiesdetails" className="smoothScroll">VIEW DETAILS</Link>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                            <img src="https://myrealhub.000webhostapp.com/images/propertypage/img01.jpg" alt="" />
                            <h1 className="title">BUILDING: 123 ON THE PARk </h1>
                            <div className="price">&#8369; 4,399/<small>month</small></div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon01.jpg' className='thumbnail' alt="" /><label>1200 sq. ft.</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon02.jpg' className='thumbnail' alt="" /><label>4 bedrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon03.jpg' className='thumbnail' alt="" /><label>2 bathrooms</label>
                            </div>
                            <div className="col-md-6">
                                <img src='https://myrealhub.000webhostapp.com/images/propertypage/icon04.jpg' className='thumbnail' alt="" /><label>2 garages</label>
                            </div>
                            <div className="property_btn">
                                <Link to="/propertiesdetails" className="smoothScroll">VIEW DETAILS</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/#" className='viewall wow fadeInUp' data-wow-delay="0.6s">VIEW ALL</a>
            </div>

            <div className="property_page03">
                <div className="container">
                    <img src="https://myrealhub.000webhostapp.com/images/propertypage/title02.png" className="title wow fadeInUp" alt="" data-wow-delay="0.6s" />
                    <div className="row">
                        <a href='/#'><div className="col-md-3 cebucity wow fadeInUp" data-wow-delay="0.6s">CEBU CITY</div></a>
                        <a href='/#'><div className="col-md-8 mandauecity wow fadeInUp" data-wow-delay="0.7s">MANDAUE CITY</div></a>
                        <a href='/#'><div className="col-md-8 talisaycity wow fadeInUp" data-wow-delay="0.8s">TALISAY CITY</div></a>
                        <a href='/#'><div className="col-md-3 danaocity wow fadeInUp" data-wow-delay="0.9s">DANAO CITY</div></a>
                    </div>
                    <a href="/#" className="viewall wow fadeInUp" data-wow-delay="0.7s">VIEW ALL</a>
                </div>
            </div>

            <div className="property_page04">
                <div className="container">
                    <img src="https://myrealhub.000webhostapp.com/images/propertypage/title03.png" className="title wow fadeInUp" alt="" data-wow-delay="0.6s" />
                    <div className="row">
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.7s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img06.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.7s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img07.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.7s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img08.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.8s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img09.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.8s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img010.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.8s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img11.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.9s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img12.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.9s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img13.jpg" alt="" /></a>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.9s">
                            <a href='/#'><img src="https://myrealhub.000webhostapp.com/images/propertypage/img14.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Properties;
