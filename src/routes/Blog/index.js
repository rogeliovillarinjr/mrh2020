import React from "react";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const Blog = () => {
    return (
        <div>
            <section id="home" className="newdev_banner" data-stellar-background-ratio="1">
                <div className="row">
                    <img src="https://myrealhub.000webhostapp.com/images/slider-image1.jpg" alt="" />
                </div>
            </section>

            <div className="home_search_properties wow fadeInUp" data-wow-delay="0.5s">
                <Search />
            </div>


            <div className="aboutuspage">
                <div className="container title">
                    <h1 className="wow fadeInUp" data-wow-delay="0.7s">BLOG</h1>
                    <p className="wow fadeInUp" data-wow-delay="0.8s">
                        Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem
       		        </p>
                </div>
            </div>

            <Footer />
        </div>
    );
};

export default Blog;
