import React from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const Home = () => {

  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500
  };

  return (
    <div>
      <section id="home" className="slider" data-stellar-background-ratio="1">
        <div className="">
          <div id="myCarousel" className="carousel slide" data-ride="carousel">
            <div className="carousel-inner">
              <div className="item active">
                <img
                  src="https://myrealhub.000webhostapp.com/images/slider-image2.jpg"
                  alt=""
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <div className="carousel-caption">
                    <h3>Camella Homes </h3>
                    <h1>House and Lot Developer Philippines</h1>
                    <Link
                      to="/"
                      className="section-btn btn btn-default smoothScroll"
                    >
                      Explore
                    </Link>
                  </div>
                </div>
              </div>

              <div className="item">
                <img
                  src="https://myrealhub.000webhostapp.com/images/slider-image3.jpg"
                  alt=""
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <div className="carousel-caption">
                    <h3>Camella Homes </h3>
                    <h1>House and Lot Developer Philippines</h1>
                    <Link
                      to="/"
                      className="section-btn btn btn-default smoothScroll"
                    >
                      Explore
                    </Link>
                  </div>
                </div>
              </div>

              <div className="item">
                <img
                  src="https://myrealhub.000webhostapp.com/images/slider-image1.jpg"
                  alt=""
                  style={{ width: "100%" }}
                />
                <div className="container">
                  <div className="carousel-caption">
                    <h3>Camella Homes </h3>
                    <h1>House and Lot Developer Philippines</h1>
                    <Link
                      to="/"
                      className="section-btn btn btn-default smoothScroll"
                    >
                      Explore
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className="home_search_properties">
        <Search />
      </div>
      <section id="about" data-stellar-background-ratio="0.5">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12">
              <div className="about-info">
                <div
                  className="section-title wow fadeInUp"
                  data-wow-delay="0.2s"
                >
                  <img
                    src="https://myrealhub.000webhostapp.com/images/trendingproperties.png"
                    style={{ width: "600px" }}
                    alt=""
                  />
                </div>

                <div className="wow fadeInUp" data-wow-delay="0.4s">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip. Nemo enim ipsam voluptatem
                    quia voluptas sit aspernatur aut odit aut fugit.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div id="menu" data-stellar-background-ratio="0.5">
        <div className="container">
          <div className="row">
            <div
              className="col-sm-12 wow fadeInUp"
              data-wow-delay="0.6s"
              style={{ width: "45%" }}
            >
              <div className="menu-thumb">
                <Link to="/">
                  <img
                    src="https://myrealhub.000webhostapp.com/images/home_trend_img01.jpg"
                    className="img-responsive"
                    style={{ height: "612px" }}
                    alt=""
                  />

                  <div className="menu-info" style={{ top: "80%" }}>
                    <div className="menu-item">
                      <h3>Rancho 2 Residences </h3>
                      <p>For sale 4 Bedroom House and Lot</p>
                    </div>
                    <div className="menu-price">
                      <span>$1,000,000</span>
                    </div>
                  </div>
                </Link>
              </div>
            </div>

            <div
              className="col-sm-6 ml-0 mb-10"
              style={{
                marginLeft: "0px",
                paddingLeft: "0px",
                marginRight: "0px",
                paddingRight: "0px",
                width: "55%",
              }}
            >
              <div
                className="col-md-5 col-sm-6 ml-0 wow fadeInUp"
                data-wow-delay="0.7s"
                style={{ marginBottom: "20px", paddingRight: "25px" }}
              >
                <div className="menu-thumb">
                  <Link to="/">
                    <img
                      src="https://myrealhub.000webhostapp.com/images/home_trend_img02.jpg"
                      className="img-responsive"
                      alt=""
                      style={{ height: "297px" }}
                    />

                    <div className="menu-info">
                      <div className="menu-item">
                        <h3>Condo</h3>
                        <p>3 beedroom/ 1 toilet</p>
                      </div>
                      <div className="menu-price">
                        <span>$100</span>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>

              <div
                className="col-md-7 col-sm-6 wow fadeInUp"
                data-wow-delay="0.8s"
                style={{
                  marginLeft: "0px",
                  paddingLeft: "0px",
                  marginRight: "0px",
                  paddingRight: "0px",
                }}
              >
                <div className="menu-thumb">
                  <Link to="/" title="Chinese Noodle">
                    <img
                      src="https://myrealhub.000webhostapp.com/images/home_trend_img03.jpg"
                      className="img-responsive"
                      alt=""
                      style={{ height: "297px" }}
                    />

                    <div className="menu-info">
                      <div className="menu-item">
                        <h3>Condo</h3>
                        <p>1 beedroom / 1 toilet</p>
                      </div>
                      <div className="menu-price">
                        <span>$100</span>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>

              <div
                className="col-md-12 col-sm-12 pt-1 wow fadeInUp"
                data-wow-delay=".9s"
              >
                <div className="menu-thumb">
                  <Link to="/" title="Project title">
                    <img
                      src="https://myrealhub.000webhostapp.com/images/home_trend_img04.jpg"
                      className="img-responsive"
                      alt=""
                      style={{ height: "295px" }}
                    />

                    <div className="menu-info">
                      <div className="menu-item">
                        <h3>3 Storey Townhouses</h3>
                        <p> (7 Units) in Teachers Village Quezon City</p>
                      </div>
                      <div className="menu-price">
                        <span>$1,000,000</span>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="viewall">
            <Link to="/">VIEW ALL</Link>
          </div>
        </div>
      </div>

      <section
        id="latest_proper about wow fadeInRight"
        data-wow-delay="0.4s"
        style={{ paddingBottom: "20px" }}
      >
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12">
              <div className="about-info">
                <div
                  className="section-title wow fadeInUp"
                  data-wow-delay="0.2s"
                >
                  <img
                    src="https://myrealhub.000webhostapp.com/images/latest_properties.png"
                    style={{ width: "600px" }}
                    alt=""
                  />
                </div>

                <div className="wow fadeInUp" data-wow-delay="0.4s">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip. Nemo enim ipsam voluptatem
                    quia voluptas sit aspernatur aut odit aut fugit.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row wow fadeInUp" data-wow-delay=".7s">
          <div
            id="myCarousel"
            className="carousel slide latest"
            data-ride="carousel"
          >

            <div className="carousel-inner">
              <div className="item active">
                <img
                  src="https://myrealhub.000webhostapp.com/images/trendingproperties_bg.jpg"
                  alt="Los Angeles"
                  style={{ width: "100%"}}
                />
                <div className="carousel-caption">
                  <h1>FOR SALE</h1>
                  <h3>MALAMBING TOWNHOMES</h3>
                  <p className="by">
                    <b>By Zobelle 88 Properties Corporation</b>
                  </p>
                  <p>Teachers Village East, Quezon City</p>
                  <p>
                    Property Type: <b>Townhouses</b>
                  </p>
                  <p>
                    Unit Sizes: <b>181.67 - 251.73 m²</b>
                  </p>
                  <p>
                    Bedroom: <b>4 - 5</b>
                  </p>
                  <p>
                    Starting From <b>₱ 12,500,000</b>
                  </p>
                  <Link to="/">View More info</Link>
                </div>
              </div>
              <div className="item ">
                <img
                  src="https://myrealhub.000webhostapp.com/images/home_trend_img03.jpg"
                  alt="Los Angeles"
                  style={{ width: "100%" }}
                />
                <div className="carousel-caption">
                  <h1>FOR SALE</h1>
                  <h3>MALAMBING TOWNHOMES</h3>
                  <p className="by">
                    <b>By Zobelle 88 Properties Corporation</b>
                  </p>
                  <p>Teachers Village East, Quezon City</p>
                  <p>
                    Property Type: <b>Townhouses</b>
                  </p>
                  <p>
                    Unit Sizes: <b>181.67 - 251.73 m²</b>
                  </p>
                  <p>
                    Bedroom: <b>4 - 5</b>
                  </p>
                  <p>
                    Starting From <b>₱ 12,500,000</b>
                  </p>
                  <Link to="/">View More info</Link>
                </div>
              </div>
              <div className="item">
                <img
                  src="https://myrealhub.000webhostapp.com/images/home_trend_img04.jpg"
                  alt="Los Angeles"
                  style={{ width: "100%" }}
                />
                <div className="carousel-caption">
                  <h1>FOR SALE</h1>
                  <h3>MALAMBING TOWNHOMES</h3>
                  <p className="by">
                    <b>By Zobelle 88 Properties Corporation</b>
                  </p>
                  <p>Teachers Village East, Quezon City</p>
                  <p>
                    Property Type: <b>Townhouses</b>
                  </p>
                  <p>
                    Unit Sizes: <b>181.67 - 251.73 m²</b>
                  </p>
                  <p>
                    Bedroom: <b>4 - 5</b>
                  </p>
                  <p>
                    Starting From <b>₱ 12,500,000</b>
                  </p>
                  <Link to="/">View More info</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div>
        <div className="home_section04">
          <div className="row">
            <div className="container cebu wow fadeInUp" data-wow-delay="0.5s">
              <div>
                <img
                  src="https://myrealhub.000webhostapp.com/images/upcomingevents.png"
                  className="img-fluid"
                  style={{ width: "600px" }}
                  alt=""
                />
              </div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip. Nemo enim ipsam voluptatem quia voluptas sit
                aspernatur aut odit aut fugit.
              </p>
            </div>
          </div>
        </div>
        <section className="home_section04v2 wow fadeInUp" data-wow-delay="0.5s">
          <Slider {...settings}>
            <div>
              <Link to="/">
                <div className='wrap' style={{ backgroundImage: "url(https://myrealhub.000webhostapp.com/images/upcomingevents03.jpg)" }}>
                  <h1>HOUSE AND LOT</h1>
                  <img src="" alt="" />
                </div>
              </Link>
            </div>
            <div>
              <Link to="/">
                <div className='wrap' style={{ backgroundImage: "url(https://myrealhub.000webhostapp.com/images/upcomingevents02.jpg)" }}>
                  <h1>CONDOMINIUM</h1>
                  <img src="" alt="" />
                </div>
              </Link>
            </div>
            <div>
              <Link to="/">
                <div className='wrap' style={{ backgroundImage: "url(https://myrealhub.000webhostapp.com/images/upcomingevents01.jpg)" }}>
                  <h1>APARTMENT</h1>
                  <img src="" alt="" />
                </div>
              </Link>
            </div>
            <div>
              <Link to="/">
                <div className='wrap' style={{ backgroundImage: "url(https://myrealhub.000webhostapp.com/images/upcomingevents03.jpg)" }}>
                  <h1>HOUSE AND LOT</h1>
                  <img src="" alt="" />
                </div>
              </Link>
            </div>
            <div>
              <Link to="/">
                <div className='wrap' style={{ backgroundImage: "url(https://myrealhub.000webhostapp.com/images/upcomingevents02.jpg)" }}>
                  <h1>CONDOMINIUM</h1>
                  <img src="" alt="" />
                </div>
              </Link>
            </div>
            <div>
              <Link to="/">
                <div className='wrap' style={{ backgroundImage: "url(https://myrealhub.000webhostapp.com/images/upcomingevents01.jpg)" }}>
                  <h1>APARTMENT</h1>
                  <img src="" alt="" />
                </div>
              </Link>
            </div>
          </Slider>
        </section>
      </div>
      <div className="home_section5 parallax">
        <div className="container">
          <div className="row">
            <div className="title wow fadeInUp" data-wow-delay="0.5s">
              <div>
                <img
                  src="https://myrealhub.000webhostapp.com/images/exploring_places_title.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <p>
                With industry-leading tools and the savviest pros in the
                business, our mission is to ethically and honestly serve our
                clients so that they can achieve the dream of home..
              </p>
            </div>
          </div>
          <div className="row">
            <div className="citylist">
              <ul>
                <li className="cebu wow fadeInUp" data-wow-delay="0.6s">
                  <Link to="/">
                    <span>CEBU CITY</span>
                  </Link>
                </li>
                <li className="mandaue wow fadeInUp" data-wow-delay="0.7s">
                  <Link to="/">
                    <span>MANDAUE CITY</span>
                  </Link>
                </li>
                <li className="naga wow fadeInUp" data-wow-delay="0.8s">
                  <Link to="/">
                    <span>NAGA CITY</span>
                  </Link>
                </li>
                <li className="talisay wow fadeInUp" data-wow-delay="0.9s">
                  <Link to="/">
                    <span>TALISAY CITY</span>
                  </Link>
                </li>
                <li className="danao wow fadeInUp" data-wow-delay="1s">
                  <Link to="/">
                    <span>DANAO CITY</span>
                  </Link>
                </li>
                <li className="bogo wow fadeInUp" data-wow-delay="1.1s">
                  <Link to="/">
                    <span>BOGO CITY</span>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="row viewall">
            <Link to="/">VIEW ALL</Link>
          </div>
        </div>
      </div>
      <div className="row home_section06">
        <div className="row pt-5 pb-5">
          <div className="container header">
            <div className="text-center">
              <img
                src="https://myrealhub.000webhostapp.com/images/featured_dev_title.png"
                width="500"
                className="wow fadeInUp"
                data-wow-delay="0.3s"
                alt=""
              />
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row text-center">
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img01.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay=".9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img02.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img03.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img04.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img05.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img06.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img07.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="#">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img08.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
            <div className="col-sm-4 pb-5">
              <Link to="/">
                <img
                  src="https://myrealhub.000webhostapp.com/images/f_img09.jpg"
                  width="200"
                  className="wow fadeInUp"
                  data-wow-delay="0.9s"
                  alt=""
                />
              </Link>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;