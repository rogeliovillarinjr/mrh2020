import React from "react";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const ResourcesSeller = () => {
  return (
    <div>
      <section id="home" className="banner newdev_banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesseller/banner.jpg" alt="" />
        </div>
      </section>

      <div className="home_search_properties">
        <Search />
      </div>

      <div className="resourcepage_seller01">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesseller/img01.jpg" alt="" />
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default ResourcesSeller;
