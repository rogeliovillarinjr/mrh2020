import React from "react";
import { Link } from "react-router-dom";

const Login = () => {
  return (<div>
    <div className="mrh_login">
      <div className="container">
        <div className="row">
          <div className="login">
            <Link to="#">LOGIN</Link>
          </div>

          <div className="needhelp">
            <Link to="#" className="fb"><img src="https://myrealhub.000webhostapp.com/images/login/fb.png" alt="" /></Link>
            <Link to="#" className="tw"><img src="https://myrealhub.000webhostapp.com/images/login/tw.png" alt="" /></Link>
            <Link to="#" className="insta"><img src="https://myrealhub.000webhostapp.com/images/login/insta.png" alt="" /></Link>
            <Link to="#" className="insta">NEED HELP?</Link>
          </div>
        </div>

        <div className="form">
          <div className="row">

            <div className="col-md-6">
              <div className="logo">
                <img src="https://myrealhub.000webhostapp.com/images/login/logo.png" alt="" />
              </div>
            </div>

            <div className="col-md-6">
              <div className="loginform">
                <div className="row top">
                  <Link to="#" className="left">Login</Link>
                  <div className="right">
                    <p>New to MyRealHub? <Link to="#">Signup</Link></p>
                  </div>
                </div>
                <div className="row">
                  <input type="text" placeholder="Phone Number / Username / Email" />
                </div>
                <div className="row">
                  <input type="password" placeholder="Password" />
                </div>
                <div className="row">
                  <input type="submit" value="LOGIN" />
                </div>
                <div className="row">
                  <Link to="#" className="forgotpass">Forgot Password</Link>
                  <Link to="#" className="loginwithsms">Login with SMS</Link>
                </div>

                <div className="row loginhr">
                  <div className="col-md-5">
                    <hr />
                  </div>
                  <div className="col-md-2">
                    <label>OR</label>
                  </div>
                  <div className="col-md-5">
                    <hr />
                  </div>
                </div>

                <div className="row social">
                  <div className="facebook">
                    <img src="https://myrealhub.000webhostapp.com/images/login/fb.png" alt="" /> Facebook
                  </div>
                  <div className="gmail">
                    <img src="https://myrealhub.000webhostapp.com/images/login/lock.png" alt="" /> Gmail
                  </div>
                </div>

              </div> {/* End of Login Form */}
            </div>

          </div>
        </div> {/* End of Form */}

      </div>
    </div>

    <div className="row mrh_login_footer">
        <div className="container">
            <div className="fmenu">
                <Link to="#">CONTACT US</Link>
                <Link to="#">ABOUT US</Link>
                <Link to="#">FAQ</Link>
            </div>
            <div className="copyr">&#169; Copyright 2020 | All Rights Reserved | Powered by MyRealHub</div>
        </div>
    </div>
  </div>
  );
};

export default Login;
