import React from "react";
import { Link } from "react-router-dom";
import "../../App.css";

const Search = () => {
    return (
        <div>
            <form className="form-inline" action="">
                <div className="form-group">
                    <input
                        type="house_lot"
                        className="form-control"
                        id="house_lot"
                        placeholder="House And Lot"
                        name="house_lot"
                    />
                </div>
                <div className="form-group">
                    <input
                        type="location"
                        className="form-control"
                        id="location"
                        placeholder="Location"
                        name="location"
                    />
                </div>
                <div className="form-group">
                    <input
                        type="forsale"
                        className="form-control"
                        id="forsale"
                        placeholder="For Sale"
                        name="forsale"
                    />
                </div>
                <Link to="/search">
                    <button type="submit" className="btn btn-success">
                        SEARCH PROPERTIES
                    </button>
                </Link>
            </form>
        </div >
    );
};

export default Search;
