import React from "react";

const HeaderProfile = () => {
    return (
        <div className="profilePageHeader">
            <div className="row">
                <div className="col-md-9">
                    <input type="text" placeholder="Search" />
                </div>
                <div className="col-md-3">
                    <div className="right">
                        <a href="/#"><img alt="" src={require('../../images/profile/crm.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/connection.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/comment.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/bell.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/profile.png')} /></a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeaderProfile;
