import React, { Component, Suspense, lazy } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navbar from "./components/layout/Navbar";
import "./App.css";

const Home = lazy(() => import('./routes/Home'));
const About = lazy(() => import('./routes/About'));
const Login = lazy(() => import('./routes/Login'));
const Register = lazy(() => import('./routes/Register'));
const NewDevelopment = lazy(() => import('./routes/NewDevelopment'));
const Properties = lazy(() => import('./routes/Properties'));
const ExplorePlaces = lazy(() => import('./routes/ExplorePlaces'));
const FeaturedDevelopers = lazy(() => import('./routes/FeaturedDevelopers'));
const ResourcesBuyer = lazy(() => import('./routes/Resources/buyer'));
const ResourcesSeller = lazy(() => import('./routes/Resources/seller'));
const PropertiesDetails = lazy(() => import('./routes/Properties/details'));
const Search = lazy(() => import('./routes/Search'));
const AboutUs = lazy(() => import('./routes/About'));
const Blog = lazy(() => import('./routes/Blog'));
const Profile = lazy(() => import('./routes/Profile'));

function header() {
  const path = window.location.pathname.split('/');
  const lastUrl = path[1];

  console.log(lastUrl);
  if (lastUrl === 'profile' || lastUrl === 'login' || lastUrl === 'register') {
    const headerx = " ";
    return headerx;
  }
  else {
    const headerx = <Navbar />;
    return headerx;
  }

}

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Suspense fallback={<div>Loading...</div>}>
          <div className="App">
            {header()}
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/about" component={About} />
              <Route path="/login" component={Login} />
              <Route path="/register" component={Register} />
              <Route path="/newdevelopment" component={NewDevelopment} />
              <Route path="/properties" component={Properties} />
              <Route path="/exploreplaces" component={ExplorePlaces} />
              <Route path="/featureddevelopers" component={FeaturedDevelopers} />
              <Route path="/resourcesbuyer" component={ResourcesBuyer} />
              <Route path="/resourcesseller" component={ResourcesSeller} />
              <Route path="/propertiesdetails" component={PropertiesDetails} />
              <Route path="/search" component={Search} />
              <Route path="/about" component={AboutUs} />
              <Route path="/blog" component={Blog} />
              <Route path="/profile" component={Profile} />
            </Switch>
          </div>
        </Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
