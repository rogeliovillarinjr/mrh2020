import React from "react";
import { Link } from "react-router-dom";
import "../../App.css";

const Navbar = () => {
    return (
        <div>
            <div className="footer pt-4 pb-4" style={{ backgroundColor: "#262e26" }}>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-3 pt-4">
                            <Link to="/" className="wow fadeInUp" data-wow-delay="0.6s">
                                <img
                                    src="https://myrealhub.000webhostapp.com/images/logo_footer.png"
                                    width="200"
                                    alt=""
                                />
                            </Link>
                        </div>
                        <div className="col-sm-9">
                            <div className="row wow fadeInUp" data-wow-delay="0.6s">
                                <ul className="nav">
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">HOME</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">ABOUT US</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">CATEGORIES</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">PROPERTIES</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">NEW DEVELOPMENTS</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">EXPLORE PLACES</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">FEATURED DEVELOPERS</Link>
                                    </li>
                                    <li className="wow fadeInUp" data-wow-delay="0.6s">
                                        <Link to="/">RESOURCES</Link>
                                    </li>
                                </ul>
                            </div>
                            <hr className="wow fadeInUp" data-wow-delay="0.6s" />
                            <div className="row">
                                <p
                                    className="copyright text-right wow fadeInUp"
                                    data-wow-delay="0.6s"
                                >
                                    &copy; copyright 2019 | All Rights Reserved | Powered by
                                    MyRealHub
                </p>
                            </div>
                            <div className="row social">
                                <ul className="nav">
                                    <li>
                                        <Link to="/" className="wow fadeInUp" data-wow-delay="0.6s">
                                            <img src="https://myrealhub.000webhostapp.com/images/facebook.png" alt="" />
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/" className="wow fadeInUp" data-wow-delay="0.6s">
                                            <img src="https://myrealhub.000webhostapp.com/images/twitter.png" alt="" />
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/" className="wow fadeInUp" data-wow-delay="0.6s">
                                            <img src="https://myrealhub.000webhostapp.com/images/google_plus.png" alt="" />
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/" className="wow fadeInUp" data-wow-delay="0.6s">
                                            <img src="https://myrealhub.000webhostapp.com/images/google.png" alt="" />
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
};

export default Navbar;
