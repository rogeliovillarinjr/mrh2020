import React from "react";

const LeftProfile = () => {
    return (
        <div className="profilePageLeft">
            <div className="row">
                <div className="logo">
                    <img alt="" src={require('../../images/logo.png')} />
                </div>
                <div className="row name">
                    <div className="row">
                        <img alt="" src={require('../../images/profile/ed5d68cb57487f370581c406dec6cd35.png')} />
                    </div>
                    <div className="row fullName">
                        Bong Go
                    </div>
                    <div className="row desig">
                        CEO
                    </div>
                    <div className="row icon">
                        <a href="/#"><img alt="" src={require('../../images/profile/date.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/envelope.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/contact-phone.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/chat.png')} /></a>
                        <a href="/#"><img alt="" src={require('../../images/profile/stand-by.png')} /></a>
                    </div>
                    <div className="pageList container">
                        <a href="/#"><img alt="" src={require('../../images/profile/dashboard.png')} /> Dashboard</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/home (1).png')} /> My Properties</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/list.png')} /> My Networks</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/tracker.png')} /> Agents</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/clipboard.png')} /> Buyer</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/lens.png')} /> Seller</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/add-button.png')} /> Developers</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/eye.png')} /> Event Calendar</a><br />
                        <a href="/#"><img alt="" src={require('../../images/profile/logout.png')} /> Logout</a><br />
                    </div>
                </div>
            </div>

        </div>
    );
};

export default LeftProfile;
