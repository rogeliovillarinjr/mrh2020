import React from "react";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const FeaturedDevelopers = () => {
  return (
    <div>
      <section id="home" className="banner newdev_banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/developers/banner.jpg" alt="" />
        </div>
      </section>

      <div className="home_search_properties wow fadeInUp" data-wow-delay="0.9s">
        <Search />
      </div>

      <div className="developers_page01">
        <div className="container">
          <img className="title wow fadeInUp" src="https://myrealhub.000webhostapp.com/images/developers/title01.png" alt="" data-wow-delay="0.9s" />
          <p className="wow fadeInUp" data-wow-delay="0.5s">
            Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dore.
     		  </p>

          <div className="developers_page02">
            <div className="row">
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <h1>AYALAND</h1>
                <p>lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum.</p>
              </div>
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.9s">
                <img src="https://myrealhub.000webhostapp.com/images/developers/img01.png" className="images" alt="" />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.9s">
                <img src="https://myrealhub.000webhostapp.com/images/developers/img02.png" className="images" alt="" />
              </div>
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <h1>KING PROPERTIES</h1>
                <p>lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <h1>CEBU LANDMASTERS</h1>
                <p>lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum.</p>
              </div>
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.9s">
                <img src="https://myrealhub.000webhostapp.com/images/developers/img03.png" className="images" alt="" />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.9s">
                <img src="https://myrealhub.000webhostapp.com/images/developers/img04.png" className="images" alt="" />
              </div>
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <h1>LATITUDE CORPORATE CENTER</h1>
                <p>lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum.</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <h1>BAYSWATER</h1>
                <p>lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum.</p>
              </div>
              <div className="col-md-6 wow fadeInUp" data-wow-delay="0.9s">
                <img src="https://myrealhub.000webhostapp.com/images/developers/img05.png" className="images" alt="" />
              </div>
            </div>
          </div>
          <div className="viewallx wow fadeInUp" data-wow-delay="0.9s">
            <a href="/#">VIEW ALL</a>
          </div>

        </div>
      </div>
      <Footer />
    </div>
  );
};

export default FeaturedDevelopers;
