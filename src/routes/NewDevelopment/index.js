import React from "react";
import { Link } from "react-router-dom";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const NewDevelopment = () => {
  return (
    <div>
      <section id="home" className="newdev_banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/newdev/banner.jpg" alt="" />
        </div>
      </section>

      <div className="home_search_properties wow fadeInUp" data-wow-delay="0.5s">
        <Search />
      </div>


      <div className="newdev_page01">
        <div className="container title wow fadeInUp" data-wow-delay="0.6s">
          <img src="https://myrealhub.000webhostapp.com/images/newdev/title01.png" alt="" />
          <p className="wow fadeInUp" data-wow-delay="0.7s">
            Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem
       		</p>
        </div>
      </div>

      <div className="newdev_page02">
        <div className="container">
          <div className="wrap">
            <h1 className="wow fadeInUp" data-wow-delay="0.8s">DESIGN OF THE DAY: <span>AIM DEVELOPMENT</span></h1>
            <div className="row">
              <div className="col-md-7">
                <div className="img01 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img01.jpg" alt="" />
                </div>
              </div>
              <div className="col-md-5">
                <div className="img02 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img02.jpg" alt="" />
                </div>
                <div className="img03 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img03.jpg" alt="" />
                </div>
                <div className="details wow fadeInUp" data-wow-delay="0.8s">
                  <Link to="/propertiesdetails" className="span"> DETAILS</Link>
                  <a href="/#" className="span">WEBSITE</a>
                  <a href="/#">PIN IT</a>
                </div>
              </div>
            </div>
          </div>
          <div className="wrap">
            <h1 className="wow fadeInUp" data-wow-delay="0.8s">DESIGN OF THE DAY: <span>AIM DEVELOPMENT</span></h1>
            <div className="row">
              <div className="col-md-7">
                <div className="img01 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img01.jpg" alt="" />
                </div>
              </div>
              <div className="col-md-5">
                <div className="img02 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img02.jpg" alt="" />
                </div>
                <div className="img03 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img03.jpg" alt="" />
                </div>
                <div className="details wow fadeInUp" data-wow-delay="0.8s">
                  <Link to="/propertiesdetails" className="span"> DETAILS</Link>
                  <a href="/#" className="span">WEBSITE</a>
                  <a href="/#">PIN IT</a>
                </div>
              </div>
            </div>
          </div>
          <div className="wrap">
            <h1 className="wow fadeInUp" data-wow-delay="0.8s">DESIGN OF THE DAY: <span>AIM DEVELOPMENT</span></h1>
            <div className="row">
              <div className="col-md-7">
                <div className="img01 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img01.jpg" alt="" />
                </div>
              </div>
              <div className="col-md-5">
                <div className="img02 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img02.jpg" alt="" />
                </div>
                <div className="img03 wow fadeInUp" data-wow-delay="0.8s">
                  <img src="https://myrealhub.000webhostapp.com/images/newdev/img03.jpg" alt="" />
                </div>
                <div className="details wow fadeInUp" data-wow-delay="0.8s">
                  <Link to="/propertiesdetails" className="span"> DETAILS</Link>
                  <a href="/#" className="span">WEBSITE</a>
                  <a href="/#">PIN IT</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="newdev_page03 wow fadeInUp" data-wow-delay="0.7s">
        <div className="container">
          <div className="col-md-6">
            <div className="left">
              <a href="/#">1</a>
              <a href="/#">2</a>
              <a href="/#">3</a>
            </div>
          </div>
          <div className="col-md-6">
            <div className="right">
              <a href="/#">SHOW ME MORE</a>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default NewDevelopment;
