import React from "react";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const ResourcesBuyer = () => {
  return (
    <div style={{ backgroundColor: '#beda98' }}>
      <section id="home" className="banner newdev_banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/banner.jpg" alt="" />
        </div>
      </section>

      <div className="home_search_properties wow fadeInUp" data-wow-delay="0.9s">
        <Search />
      </div>

      <div className="resourcepage_buyer01 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img01.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer02 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img02.jpg" alt="" />
        </div>
      </div>

      <div className="resourcepage_buyer03 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img03.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer04 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img04.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer05 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img05.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer06 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img06.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer07 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img07.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer08 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img08.jpg" alt="" />
        </div>
      </div>
      <div className="resourcepage_buyer09 wow fadeInUp" data-wow-delay="2s">
        <div className="container">
          <img src="https://myrealhub.000webhostapp.com/images/resourcesbuyer/img09.jpg" alt="" />
        </div>
      </div>
      <Footer />
    </div >
  );
};

export default ResourcesBuyer;
