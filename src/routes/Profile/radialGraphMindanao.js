import React, { Component } from "react";
import ReactApexChart from "react-apexcharts";

function Graph() {
    const series = [67];
    const options = {
        chart: {
            height: 350,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    margin: 15,
                    size: '70%',
                    imageWidth: 64,
                    imageHeight: 64,
                    imageClipped: false
                },
                dataLabels: {
                    name: {
                        show: false,
                        color: '#fff'
                    },
                    value: {
                        show: true,
                        color: '#333',
                        offsetY: 70,
                        fontSize: '22px'
                    }
                },

            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                shadeIntensity: 0,
                gradientToColors: ['#207795'],
                inverseColors: true,
                stops: [100, 100]
            }
        },
        stroke: {
            lineCap: 'round'
        },
        labels: ['Volatility'],
    };

    return (
        <div>
            <ReactApexChart options={options} series={series} type="radialBar" height={350} />
        </div>
    );

}

export default Graph;