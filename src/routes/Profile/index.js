import React from "react";
import LeftProfile from "./leftProfile";
import RightProfile from "./rightProfile";
import MiddleProfile from "./middleProfile";
import Header from "./headerProfile";

const Profile = () => {
    return (
        <div className="profilePage">
            <div className="row">
                <div className="col-md-2 profilePageMainLeft">
                    <LeftProfile />
                </div>
                <div className="col-md-10">
                    <div className="row">
                        <Header />
                    </div>
                    <div className="row profilePageMainRight">
                        <div className="col-md-9">
                            <MiddleProfile />
                        </div>
                        <div className="col-md-3">
                            <RightProfile />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Profile;
