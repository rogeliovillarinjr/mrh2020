import React from "react";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const ExplorePlaces = () => {
  return (
    <div>
      <section id="home" className="newdev_banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/exploreplaces/banner.jpg" alt="" />
        </div>
      </section>

      <div className="home_search_properties wow fadeInUp" data-wow-delay="0.9s">
        <Search />
      </div>

      <div className="exploreplaces_page01">
        <div className="container title wow fadeInUp" data-wow-delay="0.9s">
          <img src="https://myrealhub.000webhostapp.com/images/exploreplaces/title01.png" alt="" />
          <p>
            Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem Lorem ipsum dorem lorem ipsum dorem
       		</p>
        </div>
      </div>
      <div className="exploreplaces_page01">
        <div className=''>
          <div className='col-md-6'>
            <a href="/#">
              <div className='wrap01 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>CEBU</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap02 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>COMPOSTELA</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap03 wrap wow fadeInUp' data-wow-delay="0.7s">
                <label>MANILA</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap04 wrap wow fadeInUp' data-wow-delay="0.7s">
                <label>QUEZON</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap05 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>NEGROS</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap06 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>RIZAL</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap07 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>PASIG</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap08 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>SIQUIJOR</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap09 wrap wow fadeInUp' data-wow-delay="0.6s">
                <label>PAGADIAN</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap10 wrap wow fadeInUp' data-wow-delay="0.6s">
                <label>DANAO</label>
              </div>
            </a>
          </div>
          <div className='col-md-6'>
            <a href="/#">
              <div className='wrap11 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>MANDAUE</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap12 wrap wow fadeInUp' data-wow-delay="0.6s">
                <label>DAVAO</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap13 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>TAGUIG</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap14 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>SIARGAO</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap15 wrap wow fadeInUp' data-wow-delay="0.8s">
                <label>MAKATI</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap16 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>PARANAQUE</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap17 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>BOGO</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap18 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>CARMEN</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap19 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>ZAMBOANGA</label>
              </div>
            </a>
            <a href="/#">
              <div className='wrap20 wrap wow fadeInUp' data-wow-delay="0.9s">
                <label>NAGA</label>
              </div>
            </a>
          </div>

        </div>
      </div>
      <Footer />
    </div>
  );
};

export default ExplorePlaces;
