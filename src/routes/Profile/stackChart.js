import React from "react";
import ReactApexChart from "react-apexcharts";

function Graph() {
    const series = [{
        name: 'This Week',
        data: [100, 90, 130, 100, 150, 130, 200]
    }, {
        name: 'This Year',
        data: [50, 160, 70, 80, 90, 140, 120]
    }, {
        name: 'This Month',
        data: [10, 60, 30, 80, 50, 90, 80]
    }];
    const options = {
        chart: {
            height: 350,
            type: 'area'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            // type: 'datetime',
            categories: ["2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"],
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    return (
        <div>
            <ReactApexChart options={options} series={series} type="area" height={350} />
        </div>

    )

}

export default Graph;