import React from "react";
import Slider from "react-slick";
import Footer from "../../components/layout/Footer";
import Search from "../../components/layout/Search";

const Properties = () => {

  const settings = {

    customPaging: function (i) {
      return (
        <a href="/#">
          <img src={`https://myrealhub.000webhostapp.com/images/slider-image${i + 1}.jpg`} alt="" />
        </a>
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    fade: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <>
      <section id="home" className="banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/propertydetail/banner.jpg" alt="" width="100%" />
        </div>
      </section>

      <div className="home_search_properties wow fadeInUp" data-wow-delay="0.9s">
        <Search />
      </div>

      <section className="property_section_02" data-stellar-background-ratio="1">
        <div className="container">
          <div className="col-md-8">
            <section className="detailslider wow fadeInUp" data-wow-delay="0.9s">
              <Slider {...settings}>
                <div>
                  <img src={"https://myrealhub.000webhostapp.com/images/slider-image1.jpg"} alt="" />
                </div>
                <div>
                  <img src={"https://myrealhub.000webhostapp.com/images/slider-image2.jpg"} alt="" />
                </div>
                <div>
                  <img src={"https://myrealhub.000webhostapp.com/images/slider-image3.jpg"} alt="" />
                </div>
              </Slider>
            </section>

            <section className="propertydetail_section_03 wow fadeInUp" data-wow-delay="0.9s">
              <h1>FEATURES</h1>
              <ul>
                <li>3 bedroom</li>
                <li>120 sqm</li>
                <li>3 bedroom</li>
                <li>3 toilet and bath</li>
                <li>Maids Room</li>
                <li>One parking slot</li>
                <li>Few steps away from high streets</li>
                <li>PHP 32 million</li>
                <li>lorem ipsum</li>
                <li>lorem ipsum</li>
              </ul>
            </section>

            <section className="propertydetail_section_04 wow fadeInUp" data-wow-delay="0.9s">
              <h1>UNIT DETAILS</h1>
              <div className="row">
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-5">
                      Selling Price:
								</div>
                    <div className="col-md-6">
                      <label>P 32,000,000.00</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Bedrooms:
								</div>
                    <div className="col-md-6">
                      <label>3</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Bathrooms:
								</div>
                    <div className="col-md-6">
                      <label>3</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Floor Area:
								</div>
                    <div className="col-md-6">
                      <label>120 sqm</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Project:
								</div>
                    <div className="col-md-6">
                      <label>Verve Residential</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Building:
								</div>
                    <div className="col-md-6">
                      <label>N/A</label>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-md-5">
                      Floor Number:
								</div>
                    <div className="col-md-6">
                      <label>16</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Parking Slots:
								</div>
                    <div className="col-md-6">
                      <label>1</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      With Balcony
								</div>
                    <div className="col-md-6">
                      <label>Yes</label>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-5">
                      Date Listed
								</div>
                    <div className="col-md-6">
                      <label>Mar 31, 2019</label>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section className="propertydetail_section_05 wow fadeInUp" data-wow-delay="0.9s">
              <h1>PROJECT DETAILS</h1>
              <p>
                Upper Thane by Lodha is a forest estate, designed to suit all your needs and is based on the concept of an integrated ‘walk-to-everything’ township. Here you will find a grand clubhouse, school, sports courts, retail, dining and other facilities, within in the township, a short walk from your home. With over 80% open spaces and international standard air-quality, your health and well-being is well taken care of. Each residence in Upper Thane comes with views of the garden or the tree-lined avenue. Select residences also offer enchanting views of the meandering Ulhas river.
					    </p>
            </section>

            <section className="propertydetail_section_05 wow fadeInUp" data-wow-delay="0.9s">
              <h1>VIDEO</h1>
              <video width="100%" controls>
                <source src="mov_bbb.mp4" type="video/mp4" />
                <source src="mov_bbb.ogg" type="video/ogg" />
                Your browser does not support HTML video.
					    </video>
            </section>
          </div>

          <div className="col-md-4">
            <div className="propertydetail_section_07 wow fadeInUp" data-wow-delay="0.9s">
              <p>$50.000/month</p>
            </div>

            <div className="propertydetail_section_08 wow fadeInUp" data-wow-delay="0.9s">
              <p>BUILDING: 123 ON THE PARK</p>
              <div className="row">
                <div className="col-md-7">
                  <span className="price">&#8369; 4,399.00/<small>month</small></span>
                </div>
                <div className="col-md-5">
                  <img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" />
                </div>
              </div>

              <div className="row details wow fadeInUp" data-wow-delay="0.9s">
                <div className="col-md-6">
                  <span className="icon"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/icon01.jpg" alt="" /></span><label>1200 sq. ft.</label>
                </div>
                <div className="col-md-6">
                  <span className="icon"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/icon02.jpg" alt="" /></span><label>4 bedrooms</label>
                </div>
                <div className="col-md-6">
                  <span className="icon"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/icon03.jpg" alt="" /></span><label>2 bathrooms</label>
                </div>
                <div className="col-md-6">
                  <span className="icon"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/icon04.jpg" alt="" /></span><label>2 garages</label>
                </div>
              </div>
            </div>

            <div className="propertydetail_section_09 wow fadeInUp" data-wow-delay="0.9s">
              <div className="row">
                <div className="col-md-4">
                  <img src="https://www.w3schools.com/howto/img_avatar.png" className="circle" alt="" />
                </div>
                <div className="col-md-8">
                  <span className="listed">Listed by</span>
                  <span className="name">JANET LOPEZ</span>
                  <span className="since">Memeber since Aug. 11, 2015</span>
                </div>
              </div>
            </div>

            <div className="propertydetail_section_10 wow fadeInUp" data-wow-delay="0.9s">
              <div className="row">
                <label>Name</label>
                <input type="text" placeholder="Name..." />
              </div>
              <div className="row">
                <label>Phone</label>
                <input type="text" placeholder="Phone..." />
              </div>
              <div className="row">
                <label>Your Message</label>
                <textarea placeholder="message..."></textarea>
              </div>
              <div className="row">
                <input type="submit" value="SEND" />
              </div>
            </div>
          </div>
        </div>

        <section className="propertydetail_section_06 wow fadeInUp" data-wow-delay="0.9s">
          <div className="container">
            <img src="https://myrealhub.000webhostapp.com/images/propertydetail/related_properties.png" width='350px' alt="" className="wow fadeInUp" data-wow-delay="0.9s"></img> />
            <div className="row list wow fadeInUp" data-wow-delay="0.9s">
              <a href="/#">
                <div className="col-md-4" style={{ backgroundImage: `url(https://myrealhub.000webhostapp.com/images/propertydetail/img01.jpg)` }}>
                  <div className="for_rent"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" /></div>
                  <div className="building">BUILDING: 123 ON THE PARK</div>
                  <div className="per_month">43,999.00/<small>month</small></div>
                </div>
              </a>
              <a href="/#">
                <div className="col-md-4" style={{ backgroundImage: `url(https://myrealhub.000webhostapp.com/images/propertydetail/img01.jpg)` }}>
                  <div className="for_rent"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" /></div>
                  <div className="building">BUILDING: 123 ON THE PARK</div>
                  <div className="per_month">43,999.00/<small>month</small></div>
                </div>
              </a>
              <a href="/#">
                <div className="col-md-4" style={{ backgroundImage: `url(https://myrealhub.000webhostapp.com/images/propertydetail/img01.jpg)` }}>
                  <div className="for_rent"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" /></div>
                  <div className="building">BUILDING: 123 ON THE PARK</div>
                  <div className="per_month">43,999.00/<small>month</small></div>
                </div>
              </a>
              <a href="/#">
                <div className="col-md-4" style={{ backgroundImage: `url(https://myrealhub.000webhostapp.com/images/propertydetail/img01.jpg)` }}>
                  <div className="for_rent"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" /></div>
                  <div className="building">BUILDING: 123 ON THE PARK</div>
                  <div className="per_month">43,999.00/<small>month</small></div>
                </div>
              </a>
              <a href="/#">
                <div className="col-md-4" style={{ backgroundImage: `url(https://myrealhub.000webhostapp.com/images/propertydetail/img01.jpg)` }}>
                  <div className="for_rent"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" /></div>
                  <div className="building">BUILDING: 123 ON THE PARK</div>
                  <div className="per_month">43,999.00/<small>month</small></div>
                </div>
              </a>
              <a href="/#">
                <div className="col-md-4" style={{ backgroundImage: `url(https://myrealhub.000webhostapp.com/images/propertydetail/img01.jpg)` }}>
                  <div className="for_rent"><img src="https://myrealhub.000webhostapp.com/images/propertydetail/for_rent.png" alt="" /></div>
                  <div className="building">BUILDING: 123 ON THE PARK</div>
                  <div className="per_month">43,999.00/<small>month</small></div>
                </div>
              </a>
            </div>
          </div>

        </section>

        <div className="propertydetail_map wow fadeInUp" data-wow-delay="0.9s">
          <img src="https://myrealhub.000webhostapp.com/images/propertydetail/map.jpg" width="100%" alt="" />
        </div>
        <Footer />
      </section>
    </>
  );
};

export default Properties;
