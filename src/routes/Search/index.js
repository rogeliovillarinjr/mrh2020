import React from "react";
import Footer from "../../components/layout/Footer";
import Slider from "react-slick";
const Search = () => {

  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500
  };

  return (
    <div>
      <section id="home" className="banner newdev_banner" data-stellar-background-ratio="1">
        <div className="row">
          <img src="https://myrealhub.000webhostapp.com/images/search/banner.jpg" alt="" />
        </div>
      </section>

      <div className="home_search_properties">
        <form className="form-inline" action="/action_page.php">
          <div className="form-group">
            <input type="house_lot" className="form-control" id="house_lot" placeholder="House And Lot" name="house_lot" />
          </div>
          <div className="form-group">
            <input type="location" className="form-control" id="location" placeholder="Location" name="location" />
          </div>
          <div className="form-group">
            <input type="forsale" className="form-control" id="forsale" placeholder="For Sale" name="forsale" />
          </div>
          <button type="submit" className="btn btn-success">SEARCH PROPERTIES</button>
        </form>
      </div>


      <div className="search_slider">
        <div className="center slider">
          <Slider {...settings}>
            <div>
              <div className="wow fadeInUp" data-wow-delay="0.6s">
                <a href="/#"><span>Greenwood Executive Village</span><img src="https://myrealhub.000webhostapp.com/images/search/slide01.jpg" alt="" /></a>
                <div className='slide_description'>
                  <p>For Sale</p>
                  <p className="price">&#8369; 10,000,000</p>
                  <p>220 sqm</p>
                  <p>Greenwood Executive Village Danao</p>
                </div>
              </div>
            </div>
            <div>
              <div className="wow fadeInUp" data-wow-delay="0.6s">
                <a href="/#"><span>Elizabeth Homes Executive Village</span><img src="https://myrealhub.000webhostapp.com/images/search/slide02.jpg" alt="" /></a>
                <div className='slide_description'>
                  <p>For Sale</p>
                  <p className="price">&#8369; 10,000,000</p>
                  <p>220 sqm</p>
                  <p>Greenwood Executive Village Cebu</p>
                </div>
              </div>
            </div>
            <div>
              <div className="wow fadeInUp" data-wow-delay="0.6s">
                <a href="/#"><span>Greenwood Executive Village</span><img src="https://myrealhub.000webhostapp.com/images/search/slide03.jpg" alt="" /></a>
                <div className='slide_description'>
                  <p>For Sale</p>
                  <p className="price">&#8369; 10,000,000</p>
                  <p>220 sqm</p>
                  <p>Greenwood Executive Village Lahug</p>
                </div>
              </div>
            </div>
            <div>
              <div className="wow fadeInUp" data-wow-delay="0.6s">
                <a href="/#"><span>Elizabeth Homes Executive Village</span><img src="https://myrealhub.000webhostapp.com/images/search/slide01.jpg" alt="" /></a>
                <div className='slide_description'>
                  <p>For Sale</p>
                  <p className="price">&#8369; 10,000,000</p>
                  <p>220 sqm</p>
                  <p>Greenwood Executive Village Talamban</p>
                </div>
              </div>
            </div>
            <div>
              <div className="wow fadeInUp" data-wow-delay="0.6s">
                <a href="/#"><span>Greenwood Executive Village</span><img src="https://myrealhub.000webhostapp.com/images/search/slide02.jpg" alt="" /></a>
                <div className='slide_description'>
                  <p>For Sale</p>
                  <p className="price">&#8369; 10,000,000</p>
                  <p>220 sqm</p>
                  <p>Greenwood Executive Village Talisay</p>
                </div>
              </div>
            </div>
            <div>
              <div className="wow fadeInUp" data-wow-delay="0.6s">
                <a href="/#"><span>Greenwood Executive Village</span><img src="https://myrealhub.000webhostapp.com/images/search/slide03.jpg" alt="" /></a>
                <div className='slide_description'>
                  <p>For Sale</p>
                  <p className="price">&#8369; 10,000,000</p>
                  <p>220 sqm</p>
                  <p>Greenwood Executive Village Compostela</p>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>


      <div className="search_pro_near search_page">
        <div className="container">
          <div className="row">
            <div className="wow fadeInUp title" data-wow-delay="0.5s">
              <div><img src="https://myrealhub.000webhostapp.com/images/search/properties_nearby.png" className="img-fluid" alt="" width="400" /></div>
            </div>
          </div>

          <div className="row search_list">
            <div className="col-md-8">
              <div className="search_list_wrap">
                <div className="col-md-6">
                  <img src="https://myrealhub.000webhostapp.com/images/search/img01.jpg" alt="" />
                </div>
                <div className="col-md-6">
                  <h4>BUILDING: 123 on the park</h4>
                  <h5>&#8369; 4,399.00/<span>month</span></h5>

                  <div className="container-fluid pl_0">
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0">
                          <div className="row">
                            <div className="col-sm-3 pr_0">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon01.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-9 pr_0">
                              1200 sq. ft.
                                </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0">
                          <div className="row">
                            <div className="col-sm-2 pl_0 pr_0 first">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon02.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-10">
                              4 bedrooms
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0 second">
                          <div className="row">
                            <div className="col-sm-3 pr_0">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon03.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-9 pr_0">
                              2 bathrooms
                                </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0 third">
                          <div className="row">
                            <div className="col-sm-2 pl_0 pr_0 fourth">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon04.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-10">
                              2 garages
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p>Lorem ipsume dorem lorem ipsum dorem Lorem ipsume dorem lorem ipsum dorem Lorem ipsume dorem lorem ipsum dorem </p>
                </div>
              </div>

              <div className="search_list_wrap">
                <div className="col-md-6">
                  <img src="https://myrealhub.000webhostapp.com/images/search/img02.jpg" alt="" />
                </div>
                <div className="col-md-6">
                  <h4>BUILDING: 123 on the park</h4>
                  <h5>&#8369; 4,399.00/<span>month</span></h5>

                  <div className="container-fluid pl_0">
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0">
                          <div className="row">
                            <div className="col-sm-3 pr_0">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon01.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-9 pr_0">
                              1200 sq. ft.
                                </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0">
                          <div className="row">
                            <div className="col-sm-2 pl_0 pr_0 first">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon02.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-10">
                              4 bedrooms
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0 second">
                          <div className="row">
                            <div className="col-sm-3 pr_0">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon03.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-9 pr_0">
                              2 bathrooms
                                </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0 third">
                          <div className="row">
                            <div className="col-sm-2 pl_0 pr_0 fourth">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon04.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-10">
                              2 garages
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p>Lorem ipsume dorem lorem ipsum dorem Lorem ipsume dorem lorem ipsum dorem Lorem ipsume dorem lorem ipsum dorem </p>
                </div>
              </div>

              <div className="search_list_wrap">
                <div className="col-md-6">
                  <img src="https://myrealhub.000webhostapp.com/images/search/img01.jpg" alt="" />
                </div>
                <div className="col-md-6">
                  <h4>BUILDING: 123 on the park</h4>
                  <h5>&#8369; 4,399.00/<span>month</span></h5>

                  <div className="container-fluid pl_0">
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0">
                          <div className="row">
                            <div className="col-sm-3 pr_0">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon01.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-9 pr_0">
                              1200 sq. ft.
                                </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0">
                          <div className="row">
                            <div className="col-sm-2 pl_0 pr_0 first">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon02.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-10">
                              4 bedrooms
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0 second">
                          <div className="row">
                            <div className="col-sm-3 pr_0">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon03.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-9 pr_0">
                              2 bathrooms
                                </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="container-fluid pl_0 third">
                          <div className="row">
                            <div className="col-sm-2 pl_0 pr_0 fourth">
                              <img src="https://myrealhub.000webhostapp.com/images/search/icon04.png" className="img-fluid" alt="" />
                            </div>
                            <div className="col-sm-10">
                              2 garages
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p>Lorem ipsume dorem lorem ipsum dorem Lorem ipsume dorem lorem ipsum dorem Lorem ipsume dorem lorem ipsum dorem </p>
                </div>
              </div>

            </div>
            <div className="col-md-4">
              <a href="/#">
                <img src="https://myrealhub.000webhostapp.com/images/search/ads01.jpg" className="img-fluid" alt="" />
              </a>
            </div>
          </div>
        </div>
        <div className="view_all">
          <a href="/#">VIEW ALL</a>
        </div>
      </div>

      <div className="search_home_section5 parallax search_page">
        <div className="container">
          <div className="row">
            <div className="title wow fadeInUp" data-wow-delay="0.5s">
              <div><img src="https://myrealhub.000webhostapp.com/images/search/more_properties.png" className="img-fluid" alt="" /></div>
            </div>
          </div>
          <div className="row img_bottom">
            <div className="col-md-8">
              <div className="col-md-6">
                <a href="/#" className="cebu"><span>CEBU</span></a>
              </div>
              <div className="col-md-6">
                <a href="/#" className="mandaue"><span>MANDAUE</span></a>
              </div>
              <div className="col-md-6">
                <a href="/#" className="talisay"><span>TALISAY</span></a>
              </div>
              <div className="col-md-6" >
                <a href="/#" className="danao"><span>DANAO</span></a>
              </div>
            </div>
            <div className="col-md-4">
              <a href="/#"><img src="https://myrealhub.000webhostapp.com/images/search/ads02.jpg" className="img-fluid" alt="" /></a>
            </div>


          </div>

          <div className="row viewall">
            <a href="/#">VIEW ALL</a>
          </div>
        </div>
      </div>

      <div className="search_home_section6 search_page">
        <div className="container">

          <div className="col-md-6">
            <h4>GET STARTED ON BUYING YOUR NEW HOME!<br />FIND YOUR AGENT</h4>
          </div>

          <div className="col-md-6">
            <a href="/#">CLICK HERE</a>
          </div>
        </div>
      </div>
      <Footer />
    </div >
  );
};

export default Search;
